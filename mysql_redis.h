/**
 * Copyright @2019 Josin All Rights Reserved.
 * Author: Josin
 * Email : xeapplee@gmail.com
 *
 * This is the MySQL-UDF for operating the Redis DB
 * A simple way to sync data from MySQL to Redis
 */


#ifndef UDF_STR_LIBRARY_H
#define UDF_STR_LIBRARY_H

#include <stdio.h>
#include <my_global.h>
#include <my_sys.h>
#include <mysql.h>

/**
 * @brief Introduction
 * The header file for Redis
 */
#include <hircluster.h>

#define REDIS_SHM_KEY           "Redis_Single"
#define REDIS_CLUSTER_SHM_KEY   "Redis_Cluster_KEY"

#define e_free           free
#define R_STRL(s)        (s), (sizeof(s) - 1)
#define REDIS_INIT_ARGS  UDF_INIT *initid, UDF_ARGS *args, char *message
#define REDIS_INIT_ARG   UDF_INIT *initid
#define REDIS_FUNC_ARGS  UDF_INIT *initid, UDF_ARGS *args, char *result, unsigned long *length, char *is_null, char *error
#define REDIS_DEF_FUNC(name, type) \
my_bool name##_init( REDIS_INIT_ARGS );\
void    name##_deinit( REDIS_INIT_ARG );\
type    name( REDIS_FUNC_ARGS );


/**
 * @brief Introduction
 * Some universal function Macro
 */
#define RedisContextNotNull(c, v) if ( !c ) { sprintf(message, "Please call RedisConnect|RedisClusterConnect function first to obtain a connection of redis!"); return v; }
#define RedisCommandReturn(r)  if ( memcmp(r->str, "OK", 2) == 0 ) { freeReplyObject(r); return 1; } else { freeReplyObject(r); return 0; }
#define RedisCommandReturnStr(r) freeReplyObject(r);

/**
 * @brief Introduction
 * Some APIs for Redis function in MySQL UDF
 */
REDIS_DEF_FUNC(RedisConnect, long long);
REDIS_DEF_FUNC(RedisConnectFree, long long);
REDIS_DEF_FUNC(RedisSet, long long);
REDIS_DEF_FUNC(RedisExecCommand, char *);
REDIS_DEF_FUNC(RedisRawCommand, char *);

/**
 * @brief Introduction
 * Below are some RedisCluster APIs
 */
REDIS_DEF_FUNC(RedisClusterConnect, long long);
REDIS_DEF_FUNC(RedisClusterConnectFree, long long);
REDIS_DEF_FUNC(RedisClusterSet, long long);
REDIS_DEF_FUNC(RedisClusterExecCommand, char *);
REDIS_DEF_FUNC(RedisClusterRawCommand, char *);

/**
 * @brief Introduction
 * Some APIs for GET Redis List data
 */
REDIS_DEF_FUNC(RedisListIndex, char *);
REDIS_DEF_FUNC(RedisListJoin, char *);

/**
    my_bool now_usec_init( REDIS_INIT_ARGS );
    void    now_usec_deinit( REDIS_INIT_ARG );
    char   *now_usec( REDIS_FUNC_ARGS );
*/

#endif